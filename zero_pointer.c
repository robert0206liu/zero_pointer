/*
 * =====================================================================================
 *
 *       Filename:  zero_pointer.c
 *
 *    Description:  Check memset to 0 will cause pointer to be NULL or not
 *
 *        Version:  1.0
 *        Created:  2015/10/16 15:33:46
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct group
{
    char string[8];
    char* p_char;
} group_t;

int main()
{
    group_t group_a;

    memset(&group_a, 0, sizeof(group_t));

    if (group_a.string == NULL)
    {
        printf("group_a.string == NULL\n");
    }
    else
    {
        printf("group_a.string != NULL\n");
    }
    
    if (group_a.p_char == NULL)
    {
        printf("group_a.p_char == NULL\n");
    }
    else
    {
        printf("group_a.p_char != NULL\n");
    }
    
    return 0;
}
